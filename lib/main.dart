import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';

void main() {
  runApp(Animator());
}

class Animator extends StatefulWidget{
  @override
  createState() => _AnimatorState();
}

class _AnimatorState extends State<Animator> with TickerProviderStateMixin{
  AnimationController _controller;

  @override
  initState(){
    super.initState();

    _controller = AnimationController(
      value: 50.0,
      lowerBound: 50.0,
      upperBound: 300.0,
      duration: Duration(seconds: 1),
      vsync: this,
    );
  }

  Widget build(BuildContext context){
    return MaterialApp(
      home: Scaffold(
        body: GestureDetector(
          onTap:(){
            final stat = _controller.status;
            if(stat == AnimationStatus.completed){
              _controller.reverse();
            }
            else {
              _controller.animateTo(
                300.0,
                curve: Curves.bounceOut,
              );
            }
          },
          child: Center(



            child: AnimatedBuilder(
              animation: _controller,
              builder: (context, child){
              return Container(
                child: child,
                height: _controller.value,
                width: _controller.value,
              );
            },
              child: Container(
              color: Colors.blue,
              constraints: BoxConstraints.expand(),
            ),
          ),
        ),
      ),
    ),
    );
  }
}

